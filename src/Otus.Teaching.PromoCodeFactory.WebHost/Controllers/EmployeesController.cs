﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }


        /// <summary>
        /// Сохранить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> SaveNewEmployee(EmployeeCreateUpdate employee)
        {
            if (employee == null)
                return BadRequest("Заполните данные сотрудника");
            var roles = await _roleRepository.GetAllAsync();
            var employeeModel = new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = employee.Email,
                //Roles = roles.Where(x => employee.Roles.Contains(x.Name)).ToList(),
            };

            if (await _employeeRepository.SaveNewValue(employeeModel))
            {
                return Ok(employeeModel.Id);
            }

            return BadRequest("Ошибка сохранения");
        }

        /// <summary>
        /// Обновить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut("{employeeId:guid}")]
        public async Task<ActionResult<Guid>> UpdateEmployee(Guid employeeId, EmployeeCreateUpdate employee)
        {
            if (employee == null)
                return BadRequest("Заполните данные сотрудника");
            if (employeeId == default(Guid))
                return BadRequest("Укажите id сотрудника");
            var roles = await _roleRepository.GetAllAsync();
            var employeeModel = await _employeeRepository.GetByIdAsync(employeeId);
            employeeModel.FirstName = employee.FirstName;
            employeeModel.LastName = employee.LastName;
            employeeModel.Email = employee.Email;
            //employeeModel.Roles = roles.Where(x => employee.Roles.Contains(x.Name)).ToList();

            if (await _employeeRepository.UpdateValue(employeeModel))
            {
                return Ok(employeeModel.Id);
            }

            return BadRequest("Ошибка сохранения");
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{employeeId:guid}")]
        public async Task<ActionResult<bool>> DeleteEmployee(Guid employeeId)
        {
            if (employeeId == default(Guid))
                return BadRequest("Заполните данные сотрудника");

            if (await _employeeRepository.DeleteValue(employeeId))
            {
                return Ok(true);
            }

            return BadRequest("Ошибка удаления");
        }
    }
}
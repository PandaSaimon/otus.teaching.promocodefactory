﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<bool> SaveNewValue(T value)
        {
            try
            {
                Data = Data.Concat(new[] { value });
            }
            catch
            {
                return Task.FromResult(false);
            }
            return Task.FromResult(true);
        }

        public Task<bool> UpdateValue(T value)
        {
            try
            {
                Data = Data.Where(x => x.Id != value.Id);
                Data = Data.Concat(new[] { value });
            }
            catch
            {
                return Task.FromResult(false);
            }
            return Task.FromResult(true);
        }

        public Task<bool> DeleteValue(Guid id)
        {
            try
            {
                Data = Data.Where(x => x.Id != id);
            }
            catch
            {
                return Task.FromResult(false);
            }
            return Task.FromResult(true);
        }
    }
}